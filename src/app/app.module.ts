import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

// FLEX LAYOUT
import { FlexLayoutModule } from '@angular/flex-layout';

// MATERIAL MODULE
import { MaterialModule } from './material/material.module';

// SERVICES
import { ApiService } from './services/api.service';

// COMPONENTS
import { NavbarComponent } from './components/navbar/navbar.component';
import { NameFilterComponent } from './components/name-filter/name-filter.component';
import { StarsFilterComponent } from './components/stars-filter/stars-filter.component';
import { HotelCardComponent } from './components/hotel-card/hotel-card.component';
import { LoaderCardComponent } from './components/loader-card/loader-card.component';

// PIPES
import { SearchPipe } from './pipes/search/search.pipe';
import { StarsPipe } from './pipes/stars/stars.pipe';

// APP
import { AlmundoApp } from './app.component';


@NgModule({
  declarations: [
    AlmundoApp,
    NavbarComponent,
    NameFilterComponent,
    StarsFilterComponent,
    HotelCardComponent,
    LoaderCardComponent,
    SearchPipe,
    StarsPipe,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpModule,
    FormsModule,
    MaterialModule,
    FlexLayoutModule,
  ],
  providers: [
    ApiService
  ],
  bootstrap: [AlmundoApp]
})

export class AppModule { }
