import { NgModule } from '@angular/core';

import {
   MatButtonModule,
   MatToolbarModule,
   MatIconModule,
   MatTooltipModule,
   MatCheckboxModule
} from '@angular/material';


@NgModule({
   imports: [
      MatButtonModule,
      MatToolbarModule,
      MatIconModule,
      MatTooltipModule,
      MatCheckboxModule
   ],
   exports: [
      MatButtonModule,
      MatToolbarModule,
      MatIconModule,
      MatTooltipModule,
      MatCheckboxModule
   ],
})
export class MaterialModule { }