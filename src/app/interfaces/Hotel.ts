export interface Hotel {
   id: number | string;
   name: string;
   image: string;
   stars: number;
   price: number;
   amenities: Array<string>;
}