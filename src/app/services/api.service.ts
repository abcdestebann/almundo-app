import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment'
import { Http } from '@angular/http';

@Injectable({
  providedIn: 'root'
})

export class ApiService {

  private urlApiRest: string = environment.url
  private moduleRoute: string = 'api/'
  private routeHotels: string = 'hotels'
  
  constructor(
    public http: Http
  ) {

  }

  /**
   * This method make a http request to the api to get all hotels
   * @returns {Promise<any>} 
   */
  getAllHotels(): Promise<any> {
    return new Promise((resolve, reject) => {
      const url: string = `${this.urlApiRest}${this.moduleRoute}${this.routeHotels}`
      return this.http.get(url).subscribe(
        (data) => resolve(data.json()['hotels']),
        (error) => reject(error)
      )
    })
  }

}
