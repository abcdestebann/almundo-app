import { Component, OnInit, Input } from '@angular/core';
import { Hotel } from '../../interfaces/Hotel';

import { MediaChange, ObservableMedia } from '@angular/flex-layout';
@Component({
  selector: 'app-hotel-card',
  templateUrl: './hotel-card.component.html',
  styleUrls: ['./hotel-card.component.scss']
})
export class HotelCardComponent implements OnInit {

  @Input() hotel: Hotel // Component get hotel like property

  public sizeContent: boolean = false

  constructor(
    public media: ObservableMedia
  ) {
    this.watchMedia()
  }

  ngOnInit() {
    this.hotel.stars
  }

  /**
 * This method call to a observable to be listening the size of device where app was opened
 * @private
 */
  private watchMedia() {
    this.media.subscribe((change: MediaChange) => {
      if (change.mqAlias == 'xs') {
        this.sizeContent = true
      } else {
        this.sizeContent = false
      }
    }).unsubscribe();
  }

  /**
   * This method verify number of stars of hotel and return array to print every star in the view
   * @param {number} number 
   * @returns {Array<number>} 
   */
  setStars(number: number): Array<number> {
    if (number == 1) return [1]
    else if (number == 2) return [1, 2]
    else if (number == 3) return [1, 2, 3]
    else if (number == 4) return [1, 2, 3, 4]
    else if (number == 5) return [1, 2, 3, 4, 5]
  }

}
