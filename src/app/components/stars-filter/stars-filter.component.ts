import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-stars-filter',
  templateUrl: './stars-filter.component.html',
  styleUrls: ['./stars-filter.component.scss']
})
export class StarsFilterComponent implements OnInit {

  @Output() emitStars = new EventEmitter<any>() // Component emit a event when this variable is called

  public expandFilter: boolean = true

  public filterStars: Array<DataStars> = [ // Initialize the filters in array to keep control of this
    {
      name: 'Todas las estrellas',
      check: true
    },
    {
      name: '5 estrellas',
      howManyStars: [1, 2, 3, 4, 5],
      check: false
    },
    {
      name: '4 estrellas',
      howManyStars: [1, 2, 3, 4],
      check: false
    },
    {
      name: '3 estrellas',
      howManyStars: [1, 2, 3],
      check: false
    },
    {
      name: '2 estrellas',
      howManyStars: [1, 2],
      check: false
    },
    {
      name: '1 estrella',
      howManyStars: [1],
      check: false
    }
  ]


  constructor() { }

  ngOnInit() {
  }

  /**
   * This method verrify filter and after emit event to make the filter in view
   * @private
   * @param {*} event 
   * @param {DataStars} stars 
   */
  private setFilterStars(event: any, stars: DataStars) {
    if (stars.check) {
      if (stars.name == 'Todas las estrellas') {
        this.filterStars.forEach((filter) => (filter.name != 'Todas las estrellas') ? filter.check = false : '')
        this.emitStars.emit({})
      } else {
        this.emitStars.emit({ stars: stars.howManyStars.length })
        this.verifyIfAllStarsIsCheck()
      }
    } else {
      this.emitStars.emit({ starsRemove: stars.howManyStars.length })
    }
  }

  /**
   * This method verify if the filter "All stars" is check is true to change to false
   * @private
   */
  private verifyIfAllStarsIsCheck() {
    for (const filter of this.filterStars) {
      if (filter.name == 'Todas las estrellas' && filter.check) {
        filter.check = false
        return null
      }
    }
  }
}

/**
 * Interface with the data necessary to every filter of stars
 * @interface DataStars
 */
interface DataStars {
  name: string
  howManyStars?: Array<number>;
  check: boolean;
}