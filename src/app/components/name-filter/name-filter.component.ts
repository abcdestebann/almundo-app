import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-name-filter',
  templateUrl: './name-filter.component.html',
  styleUrls: ['./name-filter.component.scss']
})
export class NameFilterComponent implements OnInit {

  @Output() emitName = new EventEmitter<any>() // Component emit a event when this variable is called

  public hotelName: string = ''
  public expandFilter: boolean = true

  constructor() { }

  ngOnInit() {
  }

  /**
   * This method emit the name input by user to search a hotel
   */
  public searchHotelByName() {
    this.emitName.emit({ name: this.hotelName })
  }

}
