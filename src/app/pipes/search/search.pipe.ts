import { Pipe, PipeTransform } from '@angular/core';
import { Hotel } from '../../interfaces/Hotel';

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {

  /**
   * This method filter hotels by name 
   * @param {Array<Hotel>} hotels 
   * @param {string} value 
   * @returns {*} 
   */
  transform(hotels: Array<Hotel>, value: string): any {
    if (value) {
      return hotels.filter(element => element.name.toLowerCase().includes(value.toLowerCase()))
    } else {
      return hotels
    }
  }

}
