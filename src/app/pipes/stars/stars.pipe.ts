import { Pipe, PipeTransform } from '@angular/core';
import { Hotel } from '../../interfaces/Hotel';

@Pipe({
  name: 'stars'
})
export class StarsPipe implements PipeTransform {
  
  /**
   * This method filter hotels by stars 
   * @param {Array<Hotel>} hotels 
   * @param {Array<any>} stars 
   * @returns {*} 
   */
  transform(hotels: Array<Hotel>, stars: Array<any>): any {
    if (stars.length > 0) {
      return hotels.filter(hotel => stars.indexOf(hotel.stars) != -1)
    } else {
      return hotels
    }
  }

}
