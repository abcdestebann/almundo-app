import { Component } from '@angular/core';
import { MediaChange, ObservableMedia } from '@angular/flex-layout';
import * as _ from 'lodash'

import { ApiService } from './services/api.service';
import { Hotel } from './interfaces/Hotel';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AlmundoApp {

  public hotels: Array<Hotel> = []

  public auxHotels: Array<number> = []
  public starsSelected: Array<any> = []

  public setDropDownFilter: boolean = false
  public expandFilter: boolean = true

  public hotelNameFilter: string

  constructor(
    public apiService: ApiService,
    public media: ObservableMedia
  ) {
    this.watchMedia()
    this.getAllHotels()
  }

  /**
   * This method call to a observable to be listening the size of device where app was opened
   * @private
   */
  private watchMedia() {
    this.media.subscribe((change: MediaChange) => {
      if (change.mqAlias == 'xs') {
        this.setDropDownFilter = true
      } else {
        this.setDropDownFilter = false
      }
    }).unsubscribe();
  }

  /**
   * This method calls api service to get all hotels
   * @private
   */
  private getAllHotels() {
    this.apiService.getAllHotels()
      .then((data) => {
        this.hotels = _.sortBy(data, ['stars']).reverse()
      }).catch((error) => {
        console.log('Error', error)
      })
  }

  /**
   * This method verify if variable {setDropDownFilter} is true to expand container of filters
   */
  public expandFilters() {
    if (this.setDropDownFilter) {
      this.expandFilter = !this.expandFilter
    }
  }

  /**
   * This method set hotemName to filter hotels with this name that receive as parameter
   * @param {*} event 
   */
  public filterByName(event: any) {
    this.hotelNameFilter = event.name
  }

  /**
   * This method verify what stars are checked to make the filter by stars in all hotels
   * @param {*} event 
   */
  public filterByStars(event: any) {
    if (event.starsRemove) {
      this.starsSelected.splice(this.starsSelected.indexOf(event.starsRemove), 1)
      this.starsSelected = [...this.starsSelected]
    } else if (event.stars) {
      if (this.starsSelected.length > 0) {
        if (this.starsSelected.indexOf(event.stars) == -1) {
          this.starsSelected = [...this.starsSelected, event.stars]
        }
      } else {
        this.starsSelected = [...this.starsSelected, event.stars]
      }
    } else {
      this.starsSelected = []
    }
  }
}

